package tasks;
import java.util.Arrays;

public class tasks2 {
    
    public static void main(String[] args) {
        System.out.println("1. repeat: " + repeat("mice", 5));
        System.out.println("2. dofferenceMaxMin: " + dofferenceMaxMin(new int[] {44,32,86,19}));
        System.out.println("3. asAvgWhile: " + asAvgWhile(new int[] {1,3}));
        System.out.println("4. cumulativeSum: " + cumulativeSum(new int[] {1,2,3}));
        System.out.println("5. getDecimalPlaces: " + getDecimalPlaces("33.111"));
        System.out.println("6. Fibonacci: " + Fibonacci(3));
        System.out.println("7.isValid: " + isValid("593311"));
        System.out.println("8.isStrangeParir: " + isStrangeParir("a","a"));
        System.out.println("9.1.isPrefix: " + isPrefix("automation","auto-"));
        System.out.println("9.2.isSuffix: " + isSuffix("retrospect","spect"));
        System.out.println("10.hexLattice: \n" + hexLattice(19));
    }

    //1
    public static String repeat(String word, int number){
        String resault = "";
        // Проходит по слову и добавляет в результату букву повторенную number раз 
        for (int i = 0; i < word.length(); i++){
            for(int j = 0; j < number; j++){
                resault += word.charAt(i);
            }
        }
        return resault;
    }

    //2
    public static int dofferenceMaxMin(int[] array){
        int min = 0;
        int max = 0;
        // В цикле находит максимальное и минимальное значение в массиве
        for (int i = 0; i < array.length; i++){
            if (array[i] > max) max = array[i];
            if (array[i] < min) min = array[i];
        }
        // Возвращает разницу между максимальным и минимальным значением
        return Math.abs(max - min);
    }

    //3 
    public static boolean asAvgWhile(int[] array){
        double sum = 0;
        // Суммируем числа в массиве
        for (int i = 0; i < array.length; i++){
            sum += array[i];
        }
        // Возвращаем являетя ли число целым
        return ((sum/array.length) % 1 == 0) ? true : false;
    }

    //4 
    public static String cumulativeSum(int[] array){
        int sum = 0;
        // В цикле суммируем числа затем меняем получившаяся число
        for (int i = 0; i < array.length; i++) {
            sum += array[i]; 
            array[i] = sum; 
        }
       return Arrays.toString(array);
    }

    //5 
    public static int getDecimalPlaces(String number){
        // Если строка не имеет точки выходим из функции с 0
        if(!number.contains(".")) return 0;
        // Возвращаем число символов после точки
        return (number.length() -1 ) - (number.indexOf("."));
    }

    //6 
    public static int Fibonacci(int number){
        // Если число меньше или равно двух возвращаем число
        if (number <= 2) return number;
        // Рекурсивно вызываем функцию для нахождения числа Фибоначи
        return Fibonacci(number - 1) + Fibonacci(number - 2);
    }

    //7 
    public static boolean isValid(String number){
        // Возвращаем удовалетваряет ли строка заданным условиям
        return ((number.length() > 5) || !number.matches("[0-9]+")) ? false : true;
    }

    //8
    public static boolean isStrangeParir(String first, String second){
        // Проверка на пустые строки
        if(first.isEmpty() && second.isEmpty()) return true;
        else if(first.isEmpty() || second.isEmpty()) return false;
        // Проверяем равно ли условие 
        return (first.charAt(0) == second.charAt(second.length() - 1) && 
        second.charAt(0) == first.charAt(first.length() - 1));

    }

    //9
    public static boolean isPrefix(String word, String prefix){
        return (word.startsWith(prefix.substring(0, prefix.length()-1))) ? true : false;
    }
    public static boolean isSuffix(String word, String suffix){
        return (word.endsWith(suffix.substring(1))) ? true : false;
        
    }

    //10
    public static String hexLattice(int number){
        String resault = "";

        // Проверяем по формуле центрированного шестиугольника является ли число центрированным шестиугольником
        if(((3 + Math.sqrt(12 * number - 3)) / 6) % 1 != 0) return "Invalid";
        
        // Получаем число кругов в первой строчке
        int someVar = (int)(1 + Math.sqrt(number / 3));
        
        // Присваиваем в результат верхние строки без середины
        for (int i = someVar ; i < 2 * someVar - 1; i++) {
            resault += " ".repeat(2 * someVar - 2 - i) + " o".repeat(i) + System.lineSeparator();
        }
        // Присваиваем в результат значение средней строки
        resault += "o " + "o ".repeat( 2 * someVar - 2) + System.lineSeparator();
        
        // Присваиваем в результат нижних строк
        for (int i = 2 * someVar - 2; i >= someVar; i--) {
            resault += " ".repeat(2 * someVar - 2 - i) + " o".repeat(i) + System.lineSeparator();
        }
        return resault;
    }
}