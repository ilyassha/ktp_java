package tasks;

public class tasks1 {
    
    public static void main(String[] args) {
    
        System.out.println("1. remaider: " + remaider(5, 5));
        System.out.println("2. triArea: " + triArea(7,4 ));
        System.out.println("3. animals: " + animals(1,2,3));
        System.out.println("4. profitablegamble: " + profitablegamble(0.9,1,2));
        System.out.println("5. operation: " + operation(15,11,11));
        System.out.println("6.ctoa: " + ctoa('A'));
        System.out.println("7.addUpTo: " + addUpTo(3));
        System.out.println("8.nextEdge: " + nextEdge(9,2));
        System.out.println("9.sumOfCubes: " + sumOfCubes(new int[] {1,5,9}));
        System.out.println("10.abcmath: " + abcmath(5,2,1));
        
    }

    // 1
    public static int remaider(int first, int second){
        //Возвращает остаток от деления
        return first % second;
    }

    //2
    public static int triArea(int base, int height){
        // Возвращает площадь треугольника по формуле
        return (base * height)/2;
    }

    //3
    public static int animals(int chickens, int cows, int pigs){
         return (chickens * 2) + (cows * 4) + (pigs * 4);
    }

    //4
    public static boolean profitablegamble(double prob, double prize, double pay){
        return (prob * prize  > pay) ? true : false;
    }

    //5
    public static String operation(int N, int a, int b){

        if((a + b) == N){
            return "added";
        }
        else if ((a - b) == N){
            return "subtracted";
        }
        else if ((a / b) == N){
            return "divided";
        }
        else if ((a * b) == N){
            return "multiplied";
        }
        else{
            return "none";
        }
    }

    //6
    public static int ctoa(char Symbol ){
        // Возвращает преобразованный в int символ - ASCII код
        return (int)Symbol;
    }

    //7
    public static int addUpTo(int num){
        // Циклом складываем числа и возвращаем результат
        int result = 0;
        for (int i = 1; i <= num; i++) {
            result = result + i;
        }
        return result;
    }

    //8
    public static int nextEdge(int side1, int side2){
        return (side1+side2) - 1;
    }

    //9
    public static int sumOfCubes(int[] nums){
        // Циклом проходимся по массиву и суммируется квадраты цифр
        int resault = 0;
        for (int i = 0; i < nums.length; i++){
            resault += Math.pow(nums[i],3);
        }
        return resault;
    }

    //10
    public static boolean abcmath(int a, int b, int c){
        // Циклом складываем А 
        int resault = a;
        for (int i = 0; i < b; i++){
            a = resault;
            resault += a;
        }
        // Возвращаем результат деления А на С
        return (resault % c == 0) ? true : false;
    }
}
